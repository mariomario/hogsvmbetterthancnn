# HOGSVMbetterthanCNN

As our neural net suffers, converging to a local minimum where it is unable to effectively predict the spectral index of turbulence in Alessandro's simulations, a simple and quite dated approach, Histogram of Oriented Gradients, easily outperforms it